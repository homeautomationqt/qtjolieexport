/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "jolieexportclient.h"

#include "util.h"

#include <QtJolie/Client>
#include <QtJolie/Message>
#include <QtJolie/PendingReply>
#include <QtJolie/PendingCallWatcher>
#include <QtJolie/Value>

#include <QtCore/QString>
#include <QtCore/QDebug>

class JolieExportClientPrivate
{
public:
    
    Jolie::Client *mClient;
    
    QList<QByteArray> m_remoteObjectNames;

    QMap<QByteArray, Jolie::Value> m_properties;
};

JolieExportClient::JolieExportClient(QObject *parent) : QObject(parent), d(new JolieExportClientPrivate)
{
    d->mClient = 0;
}

JolieExportClient::JolieExportClient(QString address, quint16 port, QObject *parent)
: QObject(parent), d(new JolieExportClientPrivate)
{
    d->mClient = new Jolie::Client(address, port);

    Jolie::Message theMsg("/", "listObjects");
    sendJolieMessage(theMsg);
}

JolieExportClient::~JolieExportClient()
{
    delete d->mClient;
    delete d;
}

void JolieExportClient::sendJolieMessage(const Jolie::Message &pMsg)
{
    Jolie::PendingReply asyncReply = d->mClient->asyncCall(pMsg);
    if (d->mClient->error() == Jolie::Client::NoError)
    {
        Jolie::PendingCallWatcher *asyncWatcher = new Jolie::PendingCallWatcher(asyncReply, this);
        connect(asyncWatcher, SIGNAL(finished(Jolie::PendingCallWatcher*)), this, SLOT(receivedMessage(Jolie::PendingCallWatcher*)), Qt::QueuedConnection);
    } else {
        clientBecomesInvalid();
    }
}

QList<QByteArray> JolieExportClient::remoteObjectNames() const
{
    return d->m_remoteObjectNames;
}

void JolieExportClient::getRawPropertyValue(const QByteArray &objectName, const QByteArray &propertyName)
{
    Jolie::Message theMsg("/" + objectName, "getProperty");
    theMsg.setData(Jolie::Value(propertyName));

    sendJolieMessage(theMsg);
}

void JolieExportClient::clientBecomesInvalid()
{
    delete d->mClient;
    d->mClient = 0;

    Q_EMIT disconnected();
}

void JolieExportClient::requestChangeNotification(const QByteArray &objectName, const QByteArray &propertyName, const Jolie::Value &currentValue)
{
    Jolie::Message theMsg("/" + objectName, "notifyChange");
    Jolie::Value parameters(propertyName);
    parameters.children("currentValue").append(currentValue);
    theMsg.setData(parameters);

    sendJolieMessage(theMsg);
}

void JolieExportClient::parseAllPropertiesAnswer(const Jolie::Message &answerMsg)
{
    if (!answerMsg.data().children("properties").empty()) {
        Jolie::Value::List propertyList = answerMsg.data().children("properties");
        const QByteArray &targetObjectName(answerMsg.resourcePath().right(answerMsg.resourcePath().length() - 1));

        for (Jolie::Value::List::iterator itProperty = propertyList.begin(); itProperty != propertyList.end(); ++itProperty) {
            d->m_properties[itProperty->toByteArray()];
            getRawPropertyValue(targetObjectName, itProperty->toByteArray());
        }
    }
}

void JolieExportClient::refreshRemoteObjectsList(const QList<QByteArray> &objects)
{
    for (QList<QByteArray>::const_iterator itObject = objects.begin(); itObject != objects.end(); ++itObject) {
        if (d->m_properties.find(*itObject) == d->m_properties.end()) {
            d->m_remoteObjectNames.append(*itObject);
            Q_EMIT receivedNewRemoteObjectName(QString::fromUtf8(itObject->constData()));

            Jolie::Message theMsg("/" + *itObject, "properties");
            sendJolieMessage(theMsg);
        }
    }
}

void JolieExportClient::receivedMessage(Jolie::PendingCallWatcher *watcher)
{
    Jolie::PendingReply asyncReply(*watcher);

    Jolie::Message answerMsg(asyncReply.reply());
    if (d->mClient && d->mClient->error() == Jolie::Client::NoError) {
        if (answerMsg.isValid()) {
            if (answerMsg.resourcePath() == "/") {
                if (answerMsg.operationName() == "listObjects") {
                    if (!answerMsg.data().children("objects").empty()) {
                        const QList<QByteArray> &newExportObjectNames(JolieExport::variantFromJolieValue<QByteArray>(answerMsg.data().children("objects")));
                        refreshRemoteObjectsList(newExportObjectNames);
                    }

                    Q_EMIT connected();

                    watcher->deleteLater();
                }
            } else {
                const QByteArray &targetObjectName(answerMsg.resourcePath().right(answerMsg.resourcePath().length() - 1));
                if (answerMsg.operationName() == "getProperty") {
                    if (answerMsg.data().isValid()) {
                        if (answerMsg.data().children("currentValue").size() == 1) {
                            if (answerMsg.data().children("currentValue").front().isValid()) {
                                d->m_properties[JolieExport::variantFromJolieValue(answerMsg.data()).toByteArray()] = answerMsg.data().children("currentValue").front();
                                requestChangeNotification(targetObjectName,
                                                          JolieExport::variantFromJolieValue(answerMsg.data()).toByteArray(),
                                                          answerMsg.data().children("currentValue").front());
                            }
                        }
                    }
                    watcher->deleteLater();
                } else {
                    if (answerMsg.operationName() == "properties") {
                        parseAllPropertiesAnswer(answerMsg);
                        watcher->deleteLater();
                    } else {
                        if (answerMsg.operationName() == "notifyChange") {
                            if (answerMsg.data().isValid() && answerMsg.data().isByteArray()) {
                                if (answerMsg.data().children("currentValue").size() == 1 && answerMsg.data().children("currentValue").front().isValid()) {
                                    QVariant newValue = JolieExport::variantFromJolieValue(answerMsg.data().children("currentValue").front());

                                    Q_EMIT remotePropertyChanged(QString::fromUtf8(targetObjectName.constData()),
                                                                 QString::fromUtf8(answerMsg.data().toByteArray().constData()),
                                                                 newValue);

                                    requestChangeNotification(targetObjectName,
                                                              answerMsg.data().toByteArray(),
                                                              answerMsg.data().children("currentValue").front());
                                }
                            }
                            watcher->deleteLater();
                        }
                    }
                }
            }
        }
    } else {
        if (d->mClient && d->mClient->error() == Jolie::Client::UnexpectedClose) {
            if (answerMsg.isValid()) {
                if (answerMsg.operationName() == "notifyChange") {
                    // there is no data in the message and so there is no way to know which variable has triggered the message
                    d->m_properties.erase(d->m_properties.begin());
                    if (d->m_properties.empty()) {
                        clientBecomesInvalid();
                    }
                }
            }
        } else {
            clientBecomesInvalid();
        }
    }    
}
