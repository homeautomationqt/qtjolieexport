/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef PROPERTY_SIGNAL_MAPPER_H
#define PROPERTY_SIGNAL_MAPPER_H

#include "qtjolieexport_export.h"

#include <QtCore/QObject>
#include <QtCore/QScopedPointer>
#include <QtCore/QList>
#include <QtCore/QString>
#include <QtCore/QMap>
#include <QtCore/QVariant>

class PropertySignalMapperPrivate;

class QTJOLIEEXPORT_EXPORT PropertySignalMapper : public QObject
{
    Q_OBJECT

public:

    PropertySignalMapper(QObject *parent = 0);

    ~PropertySignalMapper();

public Q_SLOTS:

    void addTargetObject(const QObject *target);

    QList<QString> mappedObjectsName() const;

    QMap<QString, QVariant> objectProperties(const QString &objectName) const;

Q_SIGNALS:

    void changedProperty(const QString &objectName, const QString &variableName, const QVariant &newValue);

private Q_SLOTS:

    void propertyHasChanged(const QString &variableName, const QVariant &newValue);

private:

    Q_DISABLE_COPY(PropertySignalMapper);

    QScopedPointer<PropertySignalMapperPrivate> d;

};

#endif // PROPERTY_SIGNAL_MAPPER_H
