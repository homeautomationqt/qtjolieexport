/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef SIMPLE_PROPERTY_SIGNAL_MAPPER_H
#define SIMPLE_PROPERTY_SIGNAL_MAPPER_H

#include "qtjolieexport_export.h"

#include <QtCore/QObject>
#include <QtCore/QScopedPointer>
#include <QtCore/QMap>
#include <QtCore/QVariant>
#include <QtCore/QString>

class SimplePropertySignalMapperPrivate;

class QTJOLIEEXPORT_EXPORT SimplePropertySignalMapper : public QObject
{
    Q_OBJECT

public:

    SimplePropertySignalMapper(QObject *parent = 0);

    ~SimplePropertySignalMapper();

    void setTargetObject(const QObject *target);

    const QObject* targetObject() const;

    QMap<QString, QVariant> objectProperties() const;

Q_SIGNALS:

    void changedProperty(const QString &variableName, const QVariant &newValue);

private:

    Q_DISABLE_COPY(SimplePropertySignalMapper);

    QScopedPointer<SimplePropertySignalMapperPrivate> d;

private Q_SLOTS:

    void propertyHasChanged();

};

#endif // SIMPLE_PROPERTY_SIGNAL_MAPPER_H
