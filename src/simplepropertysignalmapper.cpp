/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "simplepropertysignalmapper.h"

#include <QtCore/QMetaMethod>

class SimplePropertySignalMapperPrivate
{
public:

    SimplePropertySignalMapperPrivate() : targetObject(0), propertyMap()
    {
    }

    const QObject *targetObject;

    QMap<int, const char*> propertyMap;
};

SimplePropertySignalMapper::SimplePropertySignalMapper(QObject *parent)
    : QObject(parent), d(new SimplePropertySignalMapperPrivate)
{
}

SimplePropertySignalMapper::~SimplePropertySignalMapper()
{
}

const QObject* SimplePropertySignalMapper::targetObject() const
{
    return d->targetObject;
}

void SimplePropertySignalMapper::setTargetObject(const QObject *target)
{
    if (d->targetObject) {
        d->targetObject->disconnect(this);
        d->propertyMap.clear();
    }

    d->targetObject = target;
    QMetaMethod propertyChangedSignal = metaObject()->method(metaObject()->indexOfSlot("propertyHasChanged()"));
    const int nbProperties = d->targetObject->metaObject()->propertyCount();
    for (int i = 0; i < nbProperties; ++i) {
        const QMetaProperty &currentProperty = d->targetObject->metaObject()->property(i);
        if (currentProperty.hasNotifySignal()) {
            d->propertyMap[currentProperty.notifySignalIndex()] = currentProperty.name();
            connect(d->targetObject, currentProperty.notifySignal(), this, propertyChangedSignal);
        }
    }
}

void SimplePropertySignalMapper::propertyHasChanged()
{
    const int senderSignal = senderSignalIndex();

    if (d->propertyMap.contains(senderSignal)) {
        Q_EMIT changedProperty(QString::fromUtf8(d->propertyMap[senderSignal]), d->targetObject->property(d->propertyMap[senderSignal]));
    }
}

QMap<QString, QVariant> SimplePropertySignalMapper::objectProperties() const
{
    QMap<QString, QVariant> result;

    const int nbProperties = d->targetObject->metaObject()->propertyCount();
    for (int i = 0; i < nbProperties; ++i) {
        const QMetaProperty &currentProperty = d->targetObject->metaObject()->property(i);
        if (currentProperty.hasNotifySignal()) {
            result[QString::fromUtf8(currentProperty.name())] = currentProperty.read(d->targetObject);
        }
    }

    return result;
}

#include "simplepropertysignalmapper.moc"
