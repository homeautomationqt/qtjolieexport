/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "exporteradaptor.h"
#include "util.h"

#include <QtJolie/Message>
#include <QtJolie/Value>
#include <QtJolie/Server>

#include <QtCore/QDebug>

ExporterAdaptor::ExporterAdaptor(QObject *parent) : Jolie::AbstractAdaptor(parent)
{
}

void ExporterAdaptor::relay(Jolie::Server *server, int clientId, const Jolie::Message &message)
{
    const QByteArray &targetResourcePath = message.resourcePath();
    const QByteArray &targetObjectName = targetResourcePath.right(targetResourcePath.length() - 1);

    if (message.operationName() == "properties") {
        Q_EMIT clientRequestPropertyList(clientId, message.id(), QString::fromUtf8(targetObjectName));

        return;
    }

    if (message.operationName() == "getProperty") {
        if (message.data().isValid() && message.data().isByteArray()) {
            Q_EMIT clientRequestPropertyValue(clientId, message.id(), JolieExport::objectProperty_type(QString::fromUtf8(targetObjectName),
                                                                                                       QString::fromUtf8(message.data().toByteArray())));
            return;
        }
    }

    if (message.operationName() == "notifyChange") {
        Jolie::Message answerMsg(targetResourcePath,
                                 message.operationName(),
                                 message.id());

        if (message.data().isValid() && message.data().isByteArray()) {
            if (message.data().children("currentValue").size() == 1 && message.data().children("currentValue").front().isValid()) {
                Jolie::Value currentValue = message.data().children("currentValue").front();

                Q_EMIT clientRegisterNotifierOnChange(clientId, message.id(),
                                                      JolieExport::objectProperty_type(QString::fromUtf8(targetObjectName),
                                                                                       QString::fromUtf8(message.data().toByteArray())),
                                                      currentValue);

                return;
            }
        }
    }

    Jolie::Message answerMsg(targetResourcePath,
                             message.operationName(),
                             message.id());
    server->sendReply(clientId, answerMsg);
}

void ExporterAdaptor::clientDisconnected(int clientId)
{
    Q_EMIT clientHasBeenDisconnected(clientId);
}

#include "exporteradaptor.moc"
