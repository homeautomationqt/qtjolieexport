/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined QT_JOLIE_EXPORT_EXPORTER_H
#define QT_JOLIE_EXPORT_EXPORTER_H

#include "qtjolieexport_export.h"

#include "util.h"

#include <QtJolie/Message>

#include <QtCore/QtGlobal>
#include <QtCore/QObject>
#include <QtCore/QPair>
#include <QtCore/QByteArray>

class QByteArray;

namespace Jolie
{
class Value;

class Server;
}

class JolieExporterPrivate;

class JolieExporter : public QObject
{
    Q_OBJECT

    friend class RootAdaptor;

public:

    explicit JolieExporter(QObject *parent = 0);

Q_SIGNALS:

    void reply(int clientId, Jolie::Message message);

public Q_SLOTS:

    void exportObject(const QString &objectName, const QMap<QString, QVariant> &propertiesValues);

    void propertyChanged(const QString &objectName, const QString &variableName, const QVariant &propertyValueRequested);

    void clientDisconnected(int clientId);

    void propertyListRequested(int clientId, qint64 messageId, const QString &exportObjectName);

    void propertyValueRequested(int clientId, qint64 messageId, const JolieExport::objectProperty_type &propertyKey);

    void registerNotifierOnChange(int clientId, qint64 messageId, const JolieExport::objectProperty_type &propertyKey, Jolie::Value currentValue);

private:

    Jolie::Value propertyValue(const JolieExport::objectProperty_type &propertyKey) const;

    void sendChangeNotification(int clientId, qint64 messageId, const JolieExport::objectProperty_type &propertyKey);

    JolieExporterPrivate *d;
};

#endif
