/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined QT_JOLIE_EXPORT_UTIL_H
#define QT_JOLIE_EXPORT_UTIL_H

#include "qtjolieexport_export.h"

#include <QtJolie/Value>
#include <QtCore/QList>
#include <QtCore/QByteArray>
#include <QtCore/QVariant>
#include <QtCore/QDebug>

namespace JolieExport {

typedef QPair<QString, QString> objectProperty_type;
typedef QPair<QString, int> objectSignal_type;

inline Jolie::Value jolieValueFromVariant(const QVariant &value);

inline QVariant variantFromJolieValue(const Jolie::Value &value);

template <typename T>
QList<Jolie::Value> jolieValueFromVariant(const QList<T> &values)
{
    QList<Jolie::Value> res;

    for (typename QList<T>::const_iterator itValue = values.begin(); itValue != values.end(); ++itValue) {
        qDebug() << *itValue;
        res.push_back(jolieValueFromVariant(*itValue));
    }

    return res;
}

template <typename T>
QList<T> variantFromJolieValue(const QList<Jolie::Value> &values)
{
    QList<T> res;

    for (QList<Jolie::Value>::const_iterator itValue = values.begin(); itValue != values.end(); ++itValue) {
        res.push_back(variantFromJolieValue(*itValue).value<T>());
    }

    return res;
}

inline Jolie::Value jolieValueFromVariant(const QVariant &value)
{
    if (value.isValid()) {
        switch(value.type())
        {
        case QMetaType::Int:
            return Jolie::Value(value.toInt());
        case QMetaType::Double:
            return Jolie::Value(value.toDouble());
        case QMetaType::QByteArray:
            return Jolie::Value(value.toByteArray());
        case QMetaType::QString:
            return Jolie::Value(value.toByteArray());
        default:
            return Jolie::Value();
        }
    }

    return Jolie::Value();
}

inline QVariant variantFromJolieValue(const Jolie::Value &value)
{
    if (value.isInt()) {
       return value.toInt();
    }
    if (value.isDouble()) {
        return value.toDouble();
    }
    if (value.isByteArray()) {
        return value.toByteArray();
    }

    return QVariant();
}

}

#endif
