/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "jolieexporter.h"

#include "util.h"

#include <QtJolie/Server>
#include <QtJolie/Value>

#include <QtCore/QDebug>
#include <QtCore/QMetaObject>
#include <QtCore/QMetaProperty>
#include <QtCore/QMap>
#include <QtCore/QHash>

class JolieExporterPrivate
{
public:

    JolieExporterPrivate()
        : mClientToNotify()
    {
    }

    QMap<QString, QMap<QString, QVariant> > mVariablesCache;
    QMap<QString, QMap<QString, QList<QPair<int, qint64> > > > mClientToNotify;
};

JolieExporter::JolieExporter(QObject *parent)
    : QObject(parent), d(new JolieExporterPrivate)
{
}

void JolieExporter::propertyListRequested(int clientId, qint64 messageId, const QString &exportObjectName)
{
    Jolie::Message answerMsg((QStringLiteral("/") + exportObjectName).toUtf8(), "properties", messageId);

    Jolie::Value propertiesNames;
    propertiesNames.children("properties") = JolieExport::jolieValueFromVariant(d->mClientToNotify[exportObjectName].keys());
    answerMsg.setData(propertiesNames);

    Q_EMIT reply(clientId, answerMsg);
}

Jolie::Value JolieExporter::propertyValue(const JolieExport::objectProperty_type &propertyKey) const
{
    return JolieExport::jolieValueFromVariant(d->mVariablesCache[propertyKey.first][propertyKey.second]);
}

void JolieExporter::propertyValueRequested(int clientId, qint64 messageId, const JolieExport::objectProperty_type &propertyKey)
{
    Jolie::Message answerMsg((QStringLiteral("/") + propertyKey.first).toUtf8(), "getProperty", messageId);

    Jolie::Value replyData = Jolie::Value(propertyKey.second.toUtf8());
    Jolie::Value newPropertyValue(propertyValue(propertyKey));
    replyData.children("currentValue").append(newPropertyValue);

    answerMsg.setData(replyData);

    Q_EMIT reply(clientId, answerMsg);
}

void JolieExporter::propertyChanged(const QString &objectName, const QString &variableName, const QVariant &propertyValue)
{
    Q_UNUSED(propertyValue);
    qDebug() << __FILE__ << "[" << __LINE__ << "]" << objectName << variableName << propertyValue;

    JolieExport::objectProperty_type propertyKey(objectName, variableName);
    QList<QPair<int, qint64> > &clientsList = d->mClientToNotify[objectName][variableName];
    d->mVariablesCache[objectName][variableName] = propertyValue;
    if (!clientsList.empty()) {
        for (QList<QPair<int, qint64> >::iterator itClient = clientsList.begin(); itClient != clientsList.end(); ) {
            sendChangeNotification(itClient->first, itClient->second, propertyKey);
            itClient = clientsList.erase(itClient);
        }
    }
}

void JolieExporter::registerNotifierOnChange(int clientId, qint64 messageId, const JolieExport::objectProperty_type &propertyKey, Jolie::Value currentValue)
{
    if (propertyValue(propertyKey) == currentValue) {
        d->mClientToNotify[propertyKey.first][propertyKey.second].push_back(QPair<int, qint64>(clientId, messageId));
    } else {
        sendChangeNotification(clientId, messageId, propertyKey);
    }
}

void JolieExporter::sendChangeNotification(int clientId, qint64 messageId, const JolieExport::objectProperty_type &propertyKey)
{
    Jolie::Message answerMsg((QStringLiteral("/") + propertyKey.first).toUtf8(), "notifyChange", messageId);

    Jolie::Value parameters(propertyKey.second.toUtf8());
    Jolie::Value fromValue = propertyValue(propertyKey);
    parameters.children("currentValue").append(fromValue);
    answerMsg.setData(parameters);

    Q_EMIT reply(clientId, answerMsg);
}

void JolieExporter::clientDisconnected(int clientId)
{
    for (auto itProperty = d->mClientToNotify.begin();
         itProperty != d->mClientToNotify.end(); ++itProperty) {
        for (auto itProperty2 = itProperty->begin();
             itProperty2 != itProperty->end(); ++itProperty2) {
            auto &clientsList = itProperty2.value();
            for (auto itClient = clientsList.begin(); itClient != clientsList.end(); ) {
                if (itClient->first == clientId) {
                    itClient = clientsList.erase(itClient);
                } else {
                    ++itClient;
                }
            }
        }
    }
}

void JolieExporter::exportObject(const QString &objectName, const QMap<QString, QVariant> &propertiesValues)
{
    d->mVariablesCache[objectName] = propertiesValues;
}

#include "jolieexporter.moc"
