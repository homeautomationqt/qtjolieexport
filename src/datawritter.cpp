/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datawritter.h"
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtCore/QString>
#include <QtCore/QDebug>

const QString insertDataRequest(QStringLiteral("INSERT INTO MEA_MEASURES (VAR_VARIABLE_ID, TYP_MEASURE_ID, MEA_MEASURE_DATE, MEA_MEASURE_VALUE) VALUES (:varid, :measureId, :time, :value)"));

const QString insertVariableRequest(QStringLiteral("INSERT INTO VAR_VARIABLES_LIST (VAR_OBJECT_NAME, VAR_VARIABLE_NAME) VALUES (:objname, :varname)"));
const QString getVariableRequest(QStringLiteral("SELECT VAR_VARIABLE_ID FROM VAR_VARIABLES_LIST WHERE VAR_OBJECT_NAME = :objname AND VAR_VARIABLE_NAME = :varname"));

const QString insertMeasureTypeRequest(QStringLiteral("INSERT INTO TYP_MEASURE_TYPES (TYP_MEASURE_NAME) VALUES (:name)"));
const QString getMeasureTypeRequest(QStringLiteral("SELECT TYP_MEASURE_ID FROM TYP_MEASURE_TYPES WHERE TYP_MEASURE_NAME = :name"));

class DataWritterPrivate
{
public:
    DataWritterPrivate(const QString &dbType, const QString &dbName)
        : m_db(QSqlDatabase::addDatabase(dbType)),
          m_insertDataQuery(m_db), m_insertVariableQuery(m_db), m_getVariableQuery(m_db),
          m_insertMeasureTypeQuery(m_db), m_getMeasureTypeQuery(m_db)
    {
        m_db.setDatabaseName(dbName);
        m_db.open();
        m_insertDataQuery.clear();
        m_insertDataQuery.prepare(insertDataRequest);
        m_insertVariableQuery.clear();
        m_insertVariableQuery.prepare(insertVariableRequest);
        m_getVariableQuery.clear();
        m_getVariableQuery.prepare(getVariableRequest);
        m_insertMeasureTypeQuery.clear();
        m_insertMeasureTypeQuery.prepare(insertMeasureTypeRequest);
        m_getMeasureTypeQuery.clear();
        m_getMeasureTypeQuery.prepare(getMeasureTypeRequest);
    }

    QSqlDatabase m_db;
    QSqlQuery m_insertDataQuery;
    QSqlQuery m_insertVariableQuery;
    QSqlQuery m_getVariableQuery;
    QSqlQuery m_insertMeasureTypeQuery;
    QSqlQuery m_getMeasureTypeQuery;

    QHash<QPair<QString, QString>, QVariant> m_lastValue;
};

DataWritter::DataWritter(const QString &dbFilePath, QObject *parent)
    : QObject(parent), d(new DataWritterPrivate(QStringLiteral("QSQLITE"), dbFilePath))
{
}

DataWritter::~DataWritter()
{
    delete d;
}

void DataWritter::recordNewValue(const QString &objectName, const QString &variableName, const QDateTime &timestamp,
                                 const QString &measureName, const QVariant &measureValue)
{
    const QPair<QString, QString> databaseName(objectName, variableName);

    d->m_db.transaction();

    long long int varid = getVariableId(objectName, variableName);
    if (varid == -1) {
        recordNewVariable(objectName, variableName);
        varid = getVariableId(objectName, variableName);
    }
    long long int measureid = getMeasureType(measureName);
    if (measureid == -1) {
        recordNewMeasureType(measureName);
        measureid = getMeasureType(measureName);
    }

    d->m_insertDataQuery.bindValue(QStringLiteral(":varid"), varid);
    d->m_insertDataQuery.bindValue(QStringLiteral(":measureId"), measureid);
    d->m_insertDataQuery.bindValue(QStringLiteral(":time"), timestamp);
    d->m_insertDataQuery.bindValue(QStringLiteral(":value"), measureValue);

    if (!d->m_insertDataQuery.exec()) {
        qDebug() << d->m_insertDataQuery.lastError();
    }

    d->m_db.commit();
}

void DataWritter::recordNewVariable(const QString &objectName, const QString &variableName)
{
    d->m_insertVariableQuery.bindValue(QStringLiteral(":objname"), objectName);
    d->m_insertVariableQuery.bindValue(QStringLiteral(":varname"), variableName);

    if (!d->m_insertVariableQuery.exec()) {
        qDebug() << d->m_insertVariableQuery.lastError();
    }
}

long long int DataWritter::getVariableId(const QString &objectName, const QString &variableName) const
{
    long long int result = -1;


    d->m_getVariableQuery.bindValue(QStringLiteral(":objname"), objectName);
    d->m_getVariableQuery.bindValue(QStringLiteral(":varname"), variableName);

    if (d->m_getVariableQuery.exec()) {
        if (d->m_getVariableQuery.first()) {
            result = d->m_getVariableQuery.value(0).value<long long int>();
        }
    } else {
        qDebug() << d->m_getVariableQuery.lastError();
    }

    return result;
}

void DataWritter::recordNewMeasureType(const QString &measureName)
{
    d->m_insertMeasureTypeQuery.bindValue(QStringLiteral(":name"), measureName);

    if (!d->m_insertMeasureTypeQuery.exec()) {
        qDebug() << d->m_insertMeasureTypeQuery.lastError();
    }
}

long long int DataWritter::getMeasureType(const QString &measureName) const
{
    long long int result = -1;


    d->m_getMeasureTypeQuery.bindValue(QStringLiteral(":name"), measureName);

    if (d->m_getMeasureTypeQuery.exec()) {
        if (d->m_getMeasureTypeQuery.first()) {
            result = d->m_getMeasureTypeQuery.value(0).value<long long int>();
        }
    } else {
        qDebug() << d->m_getMeasureTypeQuery.lastError();
    }

    return result;
}
