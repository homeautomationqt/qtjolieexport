/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "rootadaptor.h"
#include "jolieexporter.h"
#include "util.h"

#include <QtCore/QDebug>
#include <QtJolie/Message>
#include <QtJolie/Value>
#include <QtJolie/Server>

RootAdaptor::RootAdaptor(QObject *parent) : Jolie::AbstractAdaptor(parent)
{
}

void RootAdaptor::relay(Jolie::Server *server, int clientId, const Jolie::Message &message)
{
    if (message.operationName() == "listObjects") {
        Q_EMIT clientRequestObjectsList(clientId, message.id());

        return;
    }

    Jolie::Message answerMsg(message.resourcePath(),
                             message.operationName(),
                             message.id());
    server->sendReply(clientId, answerMsg);
}

void RootAdaptor::clientDisconnected(int clientId)
{
    Q_EMIT clientHasBeenDisconnected(clientId);
}

#include "rootadaptor.moc"
