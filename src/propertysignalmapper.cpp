/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "propertysignalmapper.h"
#include "simplepropertysignalmapper.h"

#include <QtCore/QMap>
#include <QtCore/QSharedPointer>

class PropertySignalMapperPrivate
{
public:

    QMap<QString, QSharedPointer<SimplePropertySignalMapper> > mMappings;
};

PropertySignalMapper::PropertySignalMapper(QObject *parent)
    : QObject(parent), d(new PropertySignalMapperPrivate)
{
}

PropertySignalMapper::~PropertySignalMapper()
{
}

void PropertySignalMapper::propertyHasChanged(const QString &variableName, const QVariant &newValue)
{
    Q_EMIT changedProperty(qobject_cast<const SimplePropertySignalMapper*>(sender())->targetObject()->objectName(), variableName, newValue);
}

void PropertySignalMapper::addTargetObject(const QObject *target)
{
    if (d->mMappings.find(target->objectName()) == d->mMappings.end()) {
        d->mMappings[target->objectName()] = QSharedPointer<SimplePropertySignalMapper>(new SimplePropertySignalMapper);
        d->mMappings[target->objectName()]->setTargetObject(target);
        connect(d->mMappings[target->objectName()].data(), SIGNAL(changedProperty(const QString&, const QVariant&)), this, SLOT(propertyHasChanged(QString,QVariant)));
    }
}

QList<QString> PropertySignalMapper::mappedObjectsName() const
{
    return d->mMappings.keys();
}

QMap<QString, QVariant> PropertySignalMapper::objectProperties(const QString &objectName) const
{
    return d->mMappings[objectName]->objectProperties();
}

#include "propertysignalmapper.moc"
