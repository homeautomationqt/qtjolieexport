/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "datalogger.h"
#include "datawritter.h"
#include "propertysignalmapper.h"

#include <QtCore/QDebug>
#include <QtCore/QHash>
#include <QtCore/QPair>

class DataLoggerPrivate
{
public:

    DataLoggerPrivate(PropertySignalMapper &mapper, const QString &dbFilePath) : m_writter(dbFilePath), m_mapper(mapper)
    {
    }

    DataWritter m_writter;

    PropertySignalMapper &m_mapper;
};

DataLogger::DataLogger(PropertySignalMapper &mapper, const QString &dbFilePath, QObject *parent) : QObject(parent), d(new DataLoggerPrivate(mapper, dbFilePath))
{
    connect(&d->m_mapper, SIGNAL(changedProperty(QString,QString,QVariant)), this, SLOT(propertyChanged(QString,QString,QVariant)));
}

DataLogger::~DataLogger()
{
    delete d;
}

void DataLogger::newLoggedObject(QObject *loggedObject)
{
    connect(loggedObject, SIGNAL(destroyed(QObject*)), this, SLOT(loggedObjectIsDeleted(QObject*)));

    d->m_mapper.addTargetObject(loggedObject);
}

QList<QString> DataLogger::loggedObjectsName() const
{
    return d->m_mapper.mappedObjectsName();
}

void DataLogger::loggedObjectIsDeleted(QObject *destroyed)
{
}

void DataLogger::propertyChanged(const QString &objectName, const QString &variableName, const QVariant &newValue)
{
    d->m_writter.recordNewValue(objectName, variableName, QDateTime::currentDateTime(), QStringLiteral("test"), newValue);
}
