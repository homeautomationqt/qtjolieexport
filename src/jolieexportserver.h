/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined QT_JOLIE_EXPORT_JOLIE_EXPORT_SERVER_H
#define QT_JOLIE_EXPORT_JOLIE_EXPORT_SERVER_H

#include "qtjolieexport_export.h"

#include <QtCore/QObject>

class JolieExportServerPrivate;

class PropertySignalMapper;

namespace Jolie
{
class Value;

class Server;

class Message;
}

class QTJOLIEEXPORT_EXPORT JolieExportServer : QObject
{

    Q_OBJECT

    friend class ExporterAdaptor;

    friend class RootAdaptor;

public:
    explicit JolieExportServer(PropertySignalMapper &mapper, quint16 port = 31134, QObject *parent = 0);
    
    ~JolieExportServer();

    void newExport(QObject *exportedObject);

    QList<QString> exportedObjectsName() const;

private Q_SLOTS:

    void exportedObjectIsDeleted(QObject *destroyed);

    void reply(int clientId, const Jolie::Message &message);

    void objectsListRequested(int clientId, qint64 messageId);

private:

    JolieExportServerPrivate *d;

};

#endif
