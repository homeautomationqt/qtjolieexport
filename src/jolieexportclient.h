/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined JOLIE_EXPORT_CLIENT_H
#define JOLIE_EXPORT_CLIENT_H

#include "qtjolieexport_export.h"

#include <QtJolie/Client>
#include <QtJolie/Message>
#include <QtJolie/PendingReply>
#include <QtJolie/Value>

#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <QtCore/QMap>
#include <QtCore/QVariant>

namespace Jolie
{
    class PendingCallWatcher;
}

class JolieExportClientPrivate;

class QTJOLIEEXPORT_EXPORT JolieExportClient : public QObject
{
    Q_OBJECT

public:
    JolieExportClient(QObject *parent = 0);

    JolieExportClient(QString address, quint16 port = 31134, QObject *parent = 0);

    ~JolieExportClient();

    QList<QByteArray> remoteObjectNames() const;

Q_SIGNALS:

    void receivedNewRemoteObjectName(QString objectName);

    void remotePropertyChanged(QString objectName, QString propertyName, QVariant value);

    void connected();

    void disconnected();
    
private Q_SLOTS:

    void receivedMessage(Jolie::PendingCallWatcher*);

private:
    
    void parseAllPropertiesAnswer(const Jolie::Message&);
    
    void remoteObjectValid();
    
    void requestChangeNotification(const QByteArray &objectName, const QByteArray &propertyName, const Jolie::Value &currentValue);
    
    void getRawPropertyValue(const QByteArray &objectName, const QByteArray &propertyName);
    
    void clientBecomesInvalid();

    void sendJolieMessage(const Jolie::Message&pMsg);

    void refreshRemoteObjectsList(const QList<QByteArray> &objects);
    
    JolieExportClientPrivate *d;

};

#endif

