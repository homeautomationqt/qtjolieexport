/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined DATA_LOGGER_H
#define DATA_LOGGER_H

#include "qtjolieexport_export.h"

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QVariant>

class DataLoggerPrivate;

class PropertySignalMapper;

class QTJOLIEEXPORT_EXPORT DataLogger : public QObject
{
    Q_OBJECT

public:

    explicit DataLogger(PropertySignalMapper &mapper, const QString &dbFilePath, QObject *parent = 0);

    ~DataLogger();

    void newLoggedObject(QObject *loggedObject);

    QList<QString> loggedObjectsName() const;

private Q_SLOTS:

    void loggedObjectIsDeleted(QObject *destroyed);

    void propertyChanged(const QString &objectName, const QString &variableName, const QVariant &newValue);

private:

    DataLoggerPrivate *d;
};

#endif
