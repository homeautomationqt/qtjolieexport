/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined DATA_WRITTER_H
#define DATA_WRITTER_H

#include "qtjolieexport_export.h"

#include <QtCore/QObject>
#include <QtCore/QDateTime>
#include <QtCore/QVariant>
#include <QtCore/QString>

class DataWritterPrivate;

class DataWritter : public QObject
{
    Q_OBJECT

public:

    explicit DataWritter(const QString &dbFilePath, QObject *parent = 0);

    ~DataWritter();

public Q_SLOTS:

    void recordNewValue(const QString &objectName, const QString &variableName, const QDateTime &timestamp,
                        const QString &measureName, const QVariant &measureValue);

private:

    void recordNewVariable(const QString &objectName, const QString &variableName);

    long long int getVariableId(const QString &objectName, const QString &variableName) const;

    void recordNewMeasureType(const QString &measureName);

    long long int getMeasureType(const QString &measureName) const;

    DataWritterPrivate *d;
};

#endif
