/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#if !defined JOLIE_EXPORT_ROOT_ADAPTATOR_H
#define JOLIE_EXPORT_ROOT_ADAPTATOR_H

#include "qtjolieexport_export.h"

#include <QtJolie/AbstractAdaptor>

class JolieExporter;

class RootAdaptor : public Jolie::AbstractAdaptor
{

    Q_OBJECT
public:

    explicit RootAdaptor(QObject *parent = 0);

    void clientDisconnected(int clientId);

Q_SIGNALS:

    void clientRequestObjectsList(int clientId, qint64 messageId);

    void clientHasBeenDisconnected(int clientId);

private:
    void relay(Jolie::Server *server, int clientId, const Jolie::Message &message);

};

#endif
