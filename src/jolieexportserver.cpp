/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this library; see the file COPYING.LIB.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#include "jolieexportserver.h"

#include "jolieexporter.h"
#include "propertysignalmapper.h"

#include "exporteradaptor.h"
#include "rootadaptor.h"

#include <QtJolie/Server>
#include <QtJolie/Client>
#include <QtJolie/Value>

#include <QtCore/QScopedPointer>
#include <QtCore/QDebug>

class JolieExportServerPrivate
{
public:
  
    JolieExportServerPrivate(PropertySignalMapper &mapper, quint16 port, QObject *parent = 0) : m_server(port), m_exporter(parent), m_mapper(mapper)
    {
    }

    Jolie::Server m_server;
    JolieExporter m_exporter;
    PropertySignalMapper &m_mapper;
};

JolieExportServer::JolieExportServer(PropertySignalMapper &mapper, quint16 port, QObject *parent) : QObject(parent), d(new JolieExportServerPrivate(mapper, port, this))
{
    connect(&d->m_exporter, SIGNAL(reply(int, Jolie::Message)), this, SLOT(reply(int, const Jolie::Message&)));
    connect(&d->m_mapper, SIGNAL(changedProperty(QString,QString,QVariant)), &d->m_exporter, SLOT(propertyChanged(QString,QString,QVariant)));

    QScopedPointer<RootAdaptor> adaptor(new RootAdaptor(this));
    connect(adaptor.data(), SIGNAL(clientRequestObjectsList(int,qint64)), this, SLOT(objectsListRequested(int,qint64)));
    connect(adaptor.data(), SIGNAL(clientHasBeenDisconnected(int)), &d->m_exporter, SLOT(clientDisconnected(int)));
    d->m_server.registerAdaptor("/", adaptor.take());
}

JolieExportServer::~JolieExportServer()
{
  delete d;
}

void JolieExportServer::newExport(QObject *exportedObject)
{
    QScopedPointer<ExporterAdaptor> adaptor(new ExporterAdaptor(this));
    connect(adaptor.data(), SIGNAL(clientHasBeenDisconnected(int)), &d->m_exporter, SLOT(clientDisconnected(int)));
    connect(adaptor.data(), SIGNAL(clientRegisterNotifierOnChange(int,qint64,JolieExport::objectProperty_type,Jolie::Value)),
            &d->m_exporter, SLOT(registerNotifierOnChange(int,qint64,JolieExport::objectProperty_type,Jolie::Value)));
    connect(adaptor.data(), SIGNAL(clientRequestPropertyList(int,qint64,QString)),
            &d->m_exporter, SLOT(propertyListRequested(int,qint64,QString)));
    connect(adaptor.data(), SIGNAL(clientRequestPropertyValue(int,qint64,JolieExport::objectProperty_type)),
            &d->m_exporter, SLOT(propertyValueRequested(int,qint64,JolieExport::objectProperty_type)));
    d->m_server.registerAdaptor(static_cast<QString>(QString::fromUtf8("/") + exportedObject->objectName()).toUtf8(), adaptor.take());

    connect(exportedObject, SIGNAL(destroyed(QObject*)), this, SLOT(exportedObjectIsDeleted(QObject*)));

    d->m_mapper.addTargetObject(exportedObject);
    d->m_exporter.exportObject(exportedObject->objectName(), d->m_mapper.objectProperties(exportedObject->objectName()));
}

void JolieExportServer::exportedObjectIsDeleted(QObject *destroyed)
{
  d->m_server.unregisterAdaptor(static_cast<QString>(QString::fromUtf8("/") + destroyed->objectName()).toUtf8());
}

QList<QString> JolieExportServer::exportedObjectsName() const
{
    qDebug() << __FILE__ << "[" << __LINE__ << "]" << d->m_mapper.mappedObjectsName();
    return d->m_mapper.mappedObjectsName();
}

void JolieExportServer::reply(int clientId, const Jolie::Message &message)
{
    d->m_server.sendReply(clientId, message);
}

void JolieExportServer::objectsListRequested(int clientId, qint64 messageId)
{
    Jolie::Message answerMsg("/",
                             "listObjects",
                             messageId);
    Jolie::Value answerData;
    answerData.children("objects") = JolieExport::jolieValueFromVariant(d->m_mapper.mappedObjectsName());
    answerMsg.setData(answerData);
    d->m_server.sendReply(clientId, answerMsg);
}

