/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jolie_exporter_test.h"
#include "jolieexporter.h"

#include <QtTest/QSignalSpy>
#include <QtTest/QTest>
#include <QtCore/QString>
#include <QtCore/QDebug>

JolieExporterTest::JolieExporterTest()
{
}

void JolieExporterTest::nominalSimpleTest()
{
    JolieExporter testExporter;

    QSignalSpy spy(&testExporter, SIGNAL(reply(int,Jolie::Message)));

    testExporter.propertyChanged(QStringLiteral("testObject"), QStringLiteral("TestVariable"), QVariant(1));

    QCOMPARE(spy.count(), 0);
}

void JolieExporterTest::nominalMultiTest()
{
    JolieExporter testExporter;

    QSignalSpy spy(&testExporter, SIGNAL(reply(int,Jolie::Message)));

    testExporter.propertyChanged(QStringLiteral("testObject"), QStringLiteral("TestVariable"), QVariant());

    QCOMPARE(spy.count(), 0);
}
