/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "propertySignalMapperTest.h"
#include "propertysignalmapper.h"
#include "simplepropertysignalmapper.h"

#include <QtTest/QSignalSpy>
#include <QtTest/QTest>
#include <QtCore/QString>
#include <QtCore/QDebug>

PropertySignalMapperTest::PropertySignalMapperTest()
{
}

void PropertySignalMapperTest::nominalSimpleTest()
{
    PropertySignalMapperUtility testObject(QStringLiteral("testObject"));

    SimplePropertySignalMapper testedObject;

    testedObject.setTargetObject(&testObject);

    QSignalSpy spy(&testedObject, SIGNAL(changedProperty(const QString&, const QVariant&)));

    testObject.setProperty1(1);

    QCOMPARE(spy.count(), 1);
    QList<QVariant> theSignal1 = spy.takeFirst();
    QCOMPARE(theSignal1.length(), 2);
    QCOMPARE(theSignal1.first().toString(), QStringLiteral("property1"));
    QCOMPARE(theSignal1.last(), QVariant(1));

    testObject.setProperty2(QStringLiteral("testCase"));

    QCOMPARE(spy.count(), 1);
    QList<QVariant> theSignal2 = spy.takeFirst();
    QCOMPARE(theSignal2.length(), 2);
    QCOMPARE(theSignal2.first().toString(), QStringLiteral("property2"));
    QCOMPARE(theSignal2.last(), QVariant(QStringLiteral("testCase")));
}

void PropertySignalMapperTest::nominalMultiTest()
{
    PropertySignalMapperUtility testObject1(QStringLiteral("testObject1"));
    PropertySignalMapperUtility testObject2(QStringLiteral("testObject2"));

    PropertySignalMapper testedObject;

    testedObject.addTargetObject(&testObject1);
    testedObject.addTargetObject(&testObject2);

    QSignalSpy spy(&testedObject, SIGNAL(changedProperty(const QString&, const QString&, const QVariant&)));

    testObject1.setProperty1(1);

    {
        QCOMPARE(spy.count(), 1);
        QList<QVariant> theSignal = spy.takeFirst();
        QCOMPARE(theSignal.length(), 3);
        QCOMPARE(theSignal.at(0).toString(), QStringLiteral("testObject1"));
        QCOMPARE(theSignal.at(1).toString(), QStringLiteral("property1"));
        QCOMPARE(theSignal.at(2), QVariant(1));
    }

    testObject1.setProperty2(QStringLiteral("testCase"));

    {
        QCOMPARE(spy.count(), 1);
        QList<QVariant> theSignal = spy.takeFirst();
        QCOMPARE(theSignal.length(), 3);
        QCOMPARE(theSignal.at(0).toString(), QStringLiteral("testObject1"));
        QCOMPARE(theSignal.at(1).toString(), QStringLiteral("property2"));
        QCOMPARE(theSignal.at(2), QVariant(QStringLiteral("testCase")));
    }

    testObject2.setProperty1(1);

    {
        QCOMPARE(spy.count(), 1);
        QList<QVariant> theSignal = spy.takeFirst();
        QCOMPARE(theSignal.length(), 3);
        QCOMPARE(theSignal.at(0).toString(), QStringLiteral("testObject2"));
        QCOMPARE(theSignal.at(1).toString(), QStringLiteral("property1"));
        QCOMPARE(theSignal.at(2), QVariant(1));
    }

    testObject2.setProperty2(QStringLiteral("testCase"));

    {
        QCOMPARE(spy.count(), 1);
        QList<QVariant> theSignal = spy.takeFirst();
        QCOMPARE(theSignal.length(), 3);
        QCOMPARE(theSignal.at(0).toString(), QStringLiteral("testObject2"));
        QCOMPARE(theSignal.at(1).toString(), QStringLiteral("property2"));
        QCOMPARE(theSignal.at(2), QVariant(QStringLiteral("testCase")));
    }
}
