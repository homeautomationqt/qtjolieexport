/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QtCore/QCoreApplication>
#include <QtTest/QTest>
#include "propertySignalMapperTest.h"
#include "jolie_exporter_test.h"

int testPropertySignalMapper(int argc, char *argv[])
{
    PropertySignalMapperTest testObject;

    return QTest::qExec(&testObject, argc, argv);
}

int testJolieExporter(int argc, char *argv[])
{
    JolieExporterTest testObject;

    return QTest::qExec(&testObject, argc, argv);
}

int main(int argc, char *argv[])
{
    int result = 0;

    QCoreApplication myApp(argc, argv);

    result = testPropertySignalMapper(argc, argv);
    result = testJolieExporter(argc, argv);

    return result;
}
