/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PROPERTY_SIGNAL_MAPPER_TEST_H
#define PROPERTY_SIGNAL_MAPPER_TEST_H

#include <QtCore/QObject>
#include <QtCore/QString>

class PropertySignalMapperUtility : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int property1 READ property1 WRITE setProperty1 NOTIFY property1Changed)
    Q_PROPERTY(QString property2 READ property2 WRITE setProperty2 NOTIFY property2Changed)

public:

    explicit PropertySignalMapperUtility(const QString &theObjectName, QObject *parent = 0)
        : QObject(parent)
    {
        setObjectName(theObjectName);
    }

    int property1() const
    {
        return m_property1;
    }

    void setProperty1(int value)
    {
        m_property1 = value;
        Q_EMIT property1Changed();
    }

    const QString& property2() const
    {
        return m_property2;
    }

    void setProperty2(const QString &value)
    {
        m_property2 = value;
        Q_EMIT property2Changed();
    }

Q_SIGNALS:

    void property1Changed();

    void property2Changed();

private:

    int m_property1;

    QString m_property2;
};

class PropertySignalMapperTest: public QObject
{
    Q_OBJECT
public:

    PropertySignalMapperTest();

private Q_SLOTS:
    void nominalSimpleTest();

    void nominalMultiTest();

};

#endif
