/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "propertysignalmapper.h"
#include "jolieexportserver.h"
#include "datalogger.h"
#include "ui_manualTestUi.h"
#include "test_ui_logic.h"
#include "exported_object.h"

#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>

int main(int argc, char *argv[])
{
    QApplication theApp(argc, argv);

    QMainWindow theMainWindow;
    Ui::MainWindow *theMainWindowUi = new Ui::MainWindow;
    theMainWindowUi->setupUi(&theMainWindow);
    
    TestExportedObject theExportedObject1;
    theExportedObject1.setObjectName(QString::fromUtf8("testObject1"));
    TestExportedObject theExportedObject2;
    theExportedObject2.setObjectName(QString::fromUtf8("testObject2"));

    TestUiLogic theLogic(theMainWindowUi, &theExportedObject1, &theExportedObject2);

    PropertySignalMapper theMapper;
    JolieExportServer theServer(theMapper, 31134);
    theServer.newExport(&theExportedObject1);
    theServer.newExport(&theExportedObject2);
    DataLogger theLogger(theMapper, QStringLiteral("/home/mgallien/teleinformation_qt/test.sqlite"));
    theLogger.newLoggedObject(&theExportedObject1);
    theLogger.newLoggedObject(&theExportedObject2);

    theMainWindow.show();

    return theApp.exec();
}
