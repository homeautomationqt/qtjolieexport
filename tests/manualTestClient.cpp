/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "jolieexportclient.h"
#include "client_object.h"

#include <QtCore/QCoreApplication>
#include <QtCore/QStringList>
#include <QtCore/QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication theApp(argc, argv);

    QString serverHost = QString::fromUtf8("localhost");
    if (theApp.arguments().size() == 2) {
        serverHost = theApp.arguments().at(1);
    }
    JolieExportClient theClient(serverHost, 31134);
    TestClientObject theClientObject;
    
    theApp.connect(&theClient, SIGNAL(remotePropertyChanged(QString,QString,QVariant)), &theClientObject, SLOT(remotePropertyChanged(QString,QString,QVariant)));
    theApp.connect(&theClient, SIGNAL(connected()), &theClientObject, SLOT(clientConnected()));
    theApp.connect(&theClient, SIGNAL(disconnected()), &theClientObject, SLOT(clientDisconnected()));
    theApp.connect(&theClient, SIGNAL(receivedNewRemoteObjectName(QString)), &theClientObject, SLOT(newRemoteObjectName(QString)));

    return theApp.exec();
}
