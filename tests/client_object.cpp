/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "client_object.h"

#include <QtCore/QDebug>
#include <QtCore/QCoreApplication>

TestClientObject::TestClientObject(QObject *parent) : QObject(parent)
{
}
    
void TestClientObject::remotePropertyChanged(QString objectName, QString propertyName, QVariant value)
{
    qDebug() << objectName << "with property" << propertyName << "has changed to" << value;
}

void TestClientObject::clientConnected()
{
    qDebug() << "client connected";
}

void TestClientObject::clientDisconnected()
{
    qDebug() << "client disconnected";
    QCoreApplication::quit();
}

void TestClientObject::newRemoteObjectName(QString objectName)
{
    qDebug() << "new object" << objectName;
}
