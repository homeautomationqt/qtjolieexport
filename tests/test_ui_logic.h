/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined QT_JOLIE_EXPORT_TEST_UI_LOGIC_H_
#define QT_JOLIE_EXPORT_TEST_UI_LOGIC_H_

#include <QtCore/QObject>

namespace Ui
{
  class MainWindow;
}

class TestExportedObject;

class TestUiLogic : public QObject
{
    Q_OBJECT

public:

    TestUiLogic(Ui::MainWindow *ui, TestExportedObject *target1, TestExportedObject *target2, QObject *parent = 0);

private Q_SLOTS:

    void option1IsSelected(bool state);

    void option1IsDeselected(bool state);

    void option2IsSelected(bool state);

    void option2IsDeselected(bool state);

private:

    Ui::MainWindow *m_ui;
    
    TestExportedObject *m_target1, *m_target2;
};

#endif
