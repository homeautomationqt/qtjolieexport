/*
 * Copyright 2012-2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined QT_JOLIE_EXPORT_TEST_EXPORTED_OBJECT_H_
#define QT_JOLIE_EXPORT_TEST_EXPORTED_OBJECT_H_

#include <QtCore/QObject>

class TestExportedObject : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int optionSelected READ isOptionSelected NOTIFY optionHasBeenModified)

public:

    TestExportedObject(QObject *parent = 0);

Q_SIGNALS:

    void optionHasBeenModified();

public Q_SLOTS:

    int isOptionSelected() const;

    void setOptionSelected(int selected);

private:

    int m_optionSelected;
};

#endif
