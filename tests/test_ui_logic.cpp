/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "test_ui_logic.h"
#include "ui_manualTestUi.h"
#include "exported_object.h"

TestUiLogic::TestUiLogic(Ui::MainWindow *ui, TestExportedObject *target1, TestExportedObject *target2, QObject *parent)
: QObject(parent), m_ui(ui), m_target1(target1), m_target2(target2)
{
  connect(m_ui->value11, SIGNAL(toggled(bool)), this, SLOT(option1IsSelected(bool)));
  connect(m_ui->value21, SIGNAL(toggled(bool)), this, SLOT(option1IsDeselected(bool)));
  connect(m_ui->value12, SIGNAL(toggled(bool)), this, SLOT(option2IsSelected(bool)));
  connect(m_ui->value22, SIGNAL(toggled(bool)), this, SLOT(option2IsDeselected(bool)));
}

void TestUiLogic::option1IsSelected(bool state)
{
  if (state)
  {
    m_target1->setOptionSelected(true);
  }
}

void TestUiLogic::option1IsDeselected(bool state)
{
  if (state)
  {
    m_target1->setOptionSelected(false);
  }
}

void TestUiLogic::option2IsSelected(bool state)
{
  if (state)
  {
    m_target2->setOptionSelected(true);
  }
}

void TestUiLogic::option2IsDeselected(bool state)
{
  if (state)
  {
    m_target2->setOptionSelected(false);
  }
}
