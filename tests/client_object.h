/*
 * Copyright 2013 Matthieu Gallien <matthieu_gallien@yahoo.fr>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if !defined QT_JOLIE_EXPORT_TEST_CLIENT_OBJECT_H_
#define QT_JOLIE_EXPORT_TEST_CLIENT_OBJECT_H_

#include <QtCore/QObject>
#include <QtCore/QVariant>
#include <QtCore/QString>

class TestClientObject : public QObject
{
    Q_OBJECT

public:
    
    TestClientObject(QObject *parent = 0);
    
public Q_SLOTS:

    void clientConnected();
    
    void clientDisconnected();
    
    void remotePropertyChanged(QString objectName, QString propertyName, QVariant value);

    void newRemoteObjectName(QString objectName);
};

#endif
